package MonteCarloMini;
import java.util.concurrent.RecursiveTask;

/**
 * @author Md Shaihan Islam
 * Date created: 10 August 2023
 * Description: Class that takes an input of an array of objects of type
 * Search, as well as the number of searches specified in the form of a "lo"(0) to
 * "hi" (number of searches). Aim of class is to parallelise the code to perform all the searches 
 * to find the lowest point in a particular terrain.
 */

public class SearchParallel extends RecursiveTask<SearchInfo>{
    int lo;
    int hi;
    Search searches[];  
    static final int SEQUENTIAL_CUTOFF = 5000;

    SearchParallel(Search[] a, int l, int h){ //Constructor
        searches = a;
        lo = l;
        hi = h;
    }

    protected SearchInfo compute(){
        int min = Integer.MAX_VALUE;            //min is initially set to a very high value
        int finder = -1;                        //finder (index) is set to -1
        if ((hi-lo)<SEQUENTIAL_CUTOFF){         //if the array has less than 5000 items, 
            for (int i = lo; i<hi; i++){        //implement iteration using a thread
                int local_min = searches[i].find_valleys();
                if((!searches[i].isStopped())&&(local_min<min)) { 
                    min=local_min;
                    finder=i; 
    		    }   
            }
            if (finder >= 0) {                          //Ensures a valid result is returned
                return new SearchInfo(min, finder);
            }
            return null;
        }
        else{
            SearchParallel left = new SearchParallel(searches, lo, (hi+lo)/2);  //array is halved 
            SearchParallel right = new SearchParallel(searches, (hi+lo)/2, hi); //if there are more than 5000 items
            left.fork();
            SearchInfo rightAns = right.compute();
            SearchInfo leftAns = left.join();

            if (leftAns == null) return rightAns;           // Input validation
            if (rightAns == null) return leftAns;
            if (leftAns.getValue() < rightAns.getValue()) { //If left thread has a lower value,
                return leftAns;                             //return hop onto left thread
            }
            return rightAns;                                //otherwise hop onto right thread
        }
    }
}
