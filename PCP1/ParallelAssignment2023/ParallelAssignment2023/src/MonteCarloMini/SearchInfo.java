package MonteCarloMini;

/**
 * @author Md Shaihan Islam
 * Date created: 10 August 2023
 * Description: Stores the value (height) of a particular point in a terrain, 
 * along with its index
*/
public class SearchInfo {
    int value;
    int index;

    SearchInfo(int v, int i){
        value = v;
        index = i;
    }

    public int getValue(){
        return value;
    }

    public int getIndex(){
        return index;
    }
    
}
