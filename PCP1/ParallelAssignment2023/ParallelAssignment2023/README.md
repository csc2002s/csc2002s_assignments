# CSC2002S_Assignment_PCP1


## Introduction

A program used to find the minimum point of a specific grid and terrain using 
parallel programming. The fork/join framework is applied to achieve this.

## How to run the program

To run the program, the make command "make run" can be called in the command line/terminal. The command line parameters (rows, columns, xmin, xmax, ymin,
ymax and search density) can be found in the Makefile as well, on line 15, if need be changed.


